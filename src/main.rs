use actix_files as fs;
use actix_files::NamedFile;
use actix_web::middleware::Logger;
use actix_web::{get, post, web, App, HttpServer, Responder, Result};
use kv::{Batch, Bucket, Codec, Config, Msgpack, Store};
use nanoid::nanoid;
use serde::{Deserialize, Serialize};
use std::env;
use std::{
    thread,
    time::{Duration, SystemTime, UNIX_EPOCH},
};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Secret {
    content: String,
    max_age: u64,
    max_views: u64,
}

impl Secret {
    pub fn new(content: String, max_age: u64, max_views: u64) -> Secret {
        Secret {
            content,
            max_age,
            max_views,
        }
    }
}

#[post("/add")]
async fn insert_secret(
    secret: web::Json<Secret>,
    bucket: web::Data<Bucket<'_, String, Msgpack<Secret>>>,
) -> impl Responder {
    let id = nanoid!(6);
    let value = Msgpack(secret.into_inner());

    bucket.set(id.clone(), value).unwrap();

    id
}

fn try_get_secret(
    id: String,
    bucket: web::Data<Bucket<'_, String, Msgpack<Secret>>>,
) -> Result<String, kv::Error> {
    let mut secret = match bucket.get(id.clone())? {
        Some(secret) => secret.into_inner(),
        None => return Err(kv::Error::Message("secret not found".to_string())),
    };

    secret.max_views -= 1;
    let content = secret.content.clone();

    if secret.max_views <= 0 {
        bucket.remove(id)?;
    } else {
        bucket.set(id, Msgpack(secret))?;
    }

    Ok(content)
}

#[get("/get/{id}")]
async fn get_secret(
    web::Path(id): web::Path<String>,
    bucket: web::Data<Bucket<'_, String, Msgpack<Secret>>>,
) -> impl Responder {
    match try_get_secret(id, bucket) {
        Ok(content) => content,
        Err(error) => error.to_string(),
    }
}

#[get("/size")]
async fn size(bucket: web::Data<Bucket<'_, String, Msgpack<Secret>>>) -> impl Responder {
    bucket.len().to_string()
}

#[get("/")]
async fn index() -> Result<NamedFile> {
    Ok(NamedFile::open("./html/index.html")?)
}

fn now() -> u64 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap_or(Duration::new(0, 0))
        .as_secs()
}

async fn webmanifest() -> Result<NamedFile> {
    Ok(NamedFile::open("./html/sss.webmanifest")?)
}
async fn serviceworker() -> Result<NamedFile> {
    Ok(NamedFile::open("./html/static/sw.js")?)
}

fn clean_up(bucket: web::Data<Bucket<'_, String, Msgpack<Secret>>>) {
    loop {
        let mut batch = Batch::<String, Msgpack<Secret>>::new();

        for item in bucket.iter() {
            let item = item.unwrap();
            let key: String = item.key().unwrap();
            let value = item.value::<Msgpack<Secret>>().unwrap().into_inner();

            if value.max_age <= now() {
                batch.remove(key).unwrap();
            }
        }
        bucket.batch(batch).unwrap();
        thread::sleep(Duration::from_secs(10));
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let config = Config::new(env::var("STORE_PATH").unwrap_or("./store.db".to_string()));
    let store = Store::new(config).unwrap();

    let bucket = store
        .bucket::<String, Msgpack<Secret>>(Some("secrets"))
        .unwrap();

    let app_state = web::Data::new(bucket);

    let s = app_state.clone();

    env_logger::Builder::from_default_env().init();

    thread::spawn(move || {
        clean_up(s);
    });

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .app_data(app_state.clone())
            .service(insert_secret)
            .service(get_secret)
            .service(size)
            .service(index)
            .route("/sss.webmanifest", web::get().to(webmanifest))
            .route("/sw.js", web::get().to(serviceworker))
            .service(fs::Files::new("/static", "./html/static"))
    })
    .bind(env::var("HOST").unwrap_or("127.0.0.1:8080".to_string()))?
    .run()
    .await
}
