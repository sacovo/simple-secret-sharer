FROM rust AS build
WORKDIR /usr/src


# Create a dummy project and build the app's dependencies.
# If the Cargo.toml or Cargo.lock files have not changed,
# we can use the docker build cache and skip these (typically slow) steps.
RUN USER=root cargo new simple-secret-sharer
WORKDIR /usr/src/simple-secret-sharer
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

# Copy the source and build the application.
COPY src ./src
RUN touch src/main.rs && cargo build --release

FROM gcr.io/distroless/cc

COPY ./html/ ./html/
COPY --from=build /usr/src/simple-secret-sharer/target/release/simple-secret-sharer .

ENV HOST=0.0.0.0:8080

EXPOSE "8080"

VOLUME /data

ENV STORE_PATH=/data/

CMD ["./simple-secret-sharer"]
