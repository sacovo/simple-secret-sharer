# Simple Secret Sharer

This tool lets you share your secrets to another device, for example if you are setting up a new device and don't have access to your password manager. Or if you need to log in to one account on a foreign device.

Passwords are encrypted in the browser and only ciphertext is stored on the server. You specify how often such a ciphertext can be received from the server and for how long.

## Docker container

You can use the docker container to deploy the tool yourself, add a reverse proxy like traefik to get ssl certificates.

```yaml

version: "3"

services:
  secret-sharer:
    image: registry.gitlab.com/sacovo/simple-secret-sharer:latest
    networks:
      - web
      - default
    volumes:
      - data:/data/
    expose:
      - 8080
    labels:
      - "traefik.docker.network=web"
      - "traefik.enable=true"
      - "traefik.http.routers.secret-sharer.entrypoints=https"
      - "traefik.http.routers.secret-sharer.rule=Host(`x.sacovo.ch`)"
      - "traefik.http.routers.secret-sharer.tls.certresolver=myhttpchallenge"

volumes:
  data:

networks:
  web:
    external: true
```
