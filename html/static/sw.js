const contentToCache = [
  "/",
  "/static/style.css",
  "/static/app.js",
  "/static/cleave.min.js",
];
const cacheName = "sss-v1";

self.addEventListener("install", (e) => {
  console.log("[Service Worker] Install");
  e.waitUntil(
    (async () => {
      const cache = await caches.open(cacheName);
      console.log("[Service Worker] Caching all: app shell and content");
      await cache.addAll(contentToCache);
    })()
  );
});

self.addEventListener("fetch", (e) => {
  e.respondWith(
    (async () => {
      const r = await caches.match(e.request, { ignoreSearch: true });
      console.log(`[Service Worker] Fetching resource: ${e.request.url}`);
      if (r) {
        // Refresh the cache
        let url = new URL(e.request.url);
        url.search = "";
        console.log(`[Service Worker] Refreshing resource: ${url}`);
        let request = new Request(url);

        caches.open(cacheName).then((cache) => {
          cache.add(request).then(() => console.log("Success!"));
        });

        // Return cached response
        return r;
      }
      const response = await fetch(e.request);
      // Don't cache secrets
      if (
        response.url.search("get") == -1 &&
        response.url.search("add") == -1
      ) {
        const cache = await caches.open(cacheName);
        console.log(`[Service Worker] Caching new resource: ${e.request.url}`);
        cache.put(e.request, response.clone());
      }
      return response;
    })()
  );
});
