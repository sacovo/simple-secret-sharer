const enc = new TextEncoder("utf-8");
const dec = new TextDecoder("utf-8");

const PIN_SIZE = 4;
const PIN_GROUPS = 4;
const SALT = new Uint8Array([
  176, 184, 4, 42, 218, 103, 183, 128, 209, 105, 71, 101, 122, 187, 158, 2,
]);
const ITERATIONS = 100000;

const AES_BLOCK_SIZE = 128;

async function deriveKey(key) {
  keyMaterial = await window.crypto.subtle.importKey(
    "raw",
    enc.encode(key),
    "PBKDF2",
    false,
    ["deriveBits", "deriveKey"]
  );
  return await window.crypto.subtle.deriveKey(
    {
      name: "PBKDF2",
      salt: SALT,
      iterations: ITERATIONS,
      hash: "SHA-256",
    },
    keyMaterial,
    { name: "AES-CTR", length: AES_BLOCK_SIZE },
    false,
    ["encrypt", "decrypt"]
  );
}

function bufferToBase64(buf) {
  var binstr = Array.prototype.map
    .call(buf, function (ch) {
      return String.fromCharCode(ch);
    })
    .join("");
  return btoa(binstr);
}

async function encryptSecret(secret, key) {
  const derivedKey = await deriveKey(key);

  const encoded = enc.encode(secret);

  const counter = window.crypto.getRandomValues(new Uint8Array(16));
  const encrypted = await window.crypto.subtle.encrypt(
    { name: "AES-CTR", counter, length: AES_BLOCK_SIZE },
    derivedKey,
    encoded
  );

  return bufferToBase64(
    new Uint8Array([...counter, ...new Uint8Array(encrypted)])
  );
}
function base64ToBuffer(base64) {
  var binstr = atob(base64);
  var buf = new Uint8Array(binstr.length);
  Array.prototype.forEach.call(binstr, function (ch, i) {
    buf[i] = ch.charCodeAt(0);
  });
  return buf;
}
async function decryptSecret(encrypted, key) {
  const encoded = base64ToBuffer(encrypted);
  const counter = encoded.slice(0, 16);

  const message = encoded.slice(16);

  const derivedKey = await deriveKey(key);

  const decrypted = await window.crypto.subtle.decrypt(
    { name: "AES-CTR", counter, length: AES_BLOCK_SIZE },
    derivedKey,
    message
  );

  return dec.decode(decrypted);
}

function createPassphrase() {
  const array = new Uint16Array(PIN_GROUPS);
  window.crypto.getRandomValues(array);

  let s = "";

  array.map((x) => {
    s += String(x % Math.pow(10, PIN_SIZE)).padStart(PIN_SIZE, "0") + "-";
  });

  return s.substring(0, s.length - 1);
}

(() => {
  window.addEventListener("DOMContentLoaded", () => {
    const parsedUrl = new URL(window.location);
    secret_id.value = parsedUrl.searchParams.get("text");
  });

  createForm.addEventListener("submit", (event) => {
    event.preventDefault();

    const data = new FormData(createForm);
    const passphrase = createPassphrase();

    encryptSecret(data.get("secret"), passphrase).then((encrypted) => {
      const payload = {
        content: encrypted,
        max_age:
          Math.round(new Date().getTime() / 1000) + data.get("max_age") * 60,
        max_views: +data.get("max_views"),
      };
      fetch("/add", {
        method: "POST",
        body: JSON.stringify(payload),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.text())
        .then((text) => {
          createForm.reset();
          resultId.value = text;
          resultKey.value = passphrase;
        });
    });
  });

  receiveForm.addEventListener("submit", (event) => {
    event.preventDefault();

    const data = new FormData(receiveForm);
    const id = data.get("id");

    fetch(`/get/${id}`)
      .then((response) => response.text())
      .then((text) => (encryptedValue.value = text));
  });

  new Cleave("#keyInput", {
    delimiter: "-",
    blocks: new Array(PIN_GROUPS).fill(PIN_SIZE),
    uppercase: true,
    numericOnly: true,
  });

  encryptForm.addEventListener("submit", (event) => {
    event.preventDefault();

    const data = new FormData(encryptForm);
    const key = data.get("key");

    decryptSecret(encryptedValue.value, key).then((decrypted) => {
      decryptedValue.value = decrypted;
    });
  });

  copyValue.addEventListener("click", (event) => {
    event.preventDefault();
    navigator.clipboard.writeText(decryptedValue.value);
  });
})();

if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("/sw.js");
}
